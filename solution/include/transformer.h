#ifndef IMAGE_TRANSFORMER_TRANSFORMER_H
#define IMAGE_TRANSFORMER_TRANSFORMER_H

#include <malloc.h>
#include <stdint.h>
#include <stdio.h>

#include "bmp.h"
#include "image.h"
#include "transformer.h"

struct image rotate(struct image const source);

#endif
