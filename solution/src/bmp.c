#include "bmp.h"
#include "image.h"

uint32_t calc_width_in_bytes(uint32_t w) {
    return sizeof(struct pixel) * w;
}

uint32_t calc_padding_in_bytes(uint32_t w) {
    if ((w % 4) != 0) return 4 - w % 4;
    return 0;
}

enum read_status from_bmp(FILE *in, struct image *img, struct bmp_header *head) {
    const size_t count = 1;
    //Header
    if (fread(head, sizeof(struct bmp_header), count, in) != count) return READ_FREAD_HEADER_ERROR;
    if (!head->biSizeImage) return READ_INVALID_SIZE;
    if (!head->biWidth || !head->biHeight) return READ_INVALID_DIMENSIONS;
    if (fseek(in, (uint16_t) head->bOffBits, SEEK_SET)) return READ_FSEEK_ERROR;

    img->width = head->biWidth;
    img->height = head->biHeight;

    //Body
    const uint32_t array_width_in_bytes = calc_width_in_bytes(head->biWidth);
    const uint32_t padding = calc_padding_in_bytes(array_width_in_bytes);
    uint8_t *bytes = malloc(array_width_in_bytes);

    img->data = malloc((array_width_in_bytes) * head->biHeight);

    for (size_t i = 0; i < head->biHeight; i++) {
        if (fread(bytes, array_width_in_bytes, count, in) != count) {
            free(img->data);
            return READ_FREAD_BODY_ERROR;
        }

        fseek(in, (uint16_t) padding, SEEK_CUR);

        for (size_t j = 0; j < head->biWidth; j++) {
            img->data[head->biWidth * i + j].r = bytes[3 * j];
            img->data[head->biWidth * i + j].g = bytes[3 * j + 1];
            img->data[head->biWidth * i + j].b = bytes[3 * j + 2];
        }
    }

    free(bytes);
    return READ_OK;
}


size_t to_bmp_header(FILE *out, struct bmp_header *head, size_t w, size_t h) {
    head->biWidth = w;
    head->biHeight = h;
    head->biSizeImage = (calc_width_in_bytes(w) + calc_padding_in_bytes(calc_width_in_bytes(w))) * head->biHeight;
    head->bOffBits = sizeof(struct bmp_header);
    head->bfileSize = head->bOffBits + head->biSizeImage;
    return fwrite(head, sizeof(struct bmp_header), 1, out);
}

enum write_status to_bmp(FILE *out, struct image const *img, struct bmp_header *head) {
    const size_t count = 1;
    if (to_bmp_header(out, head, img->width, img->height) != count) return WRITE_FWRITE_HEADER_ERROR;

    //Body
    const uint32_t array_width_in_bytes = calc_width_in_bytes(img->width);
    const uint32_t padding = calc_padding_in_bytes(array_width_in_bytes);
    uint8_t *bytes = malloc(array_width_in_bytes);

    //for (size_t i = 0; i < array_width_in_bytes; i++) bytes[i] = 0;
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            bytes[3 * j] = img->data[img->width * i + j].r;
            bytes[3 * j + 1] = img->data[img->width * i + j].g;
            bytes[3 * j + 2] = img->data[img->width * i + j].b;
        }
        if (fwrite(bytes, array_width_in_bytes, count, out) != count) {
            free(bytes);
            return WRITE_FWRITE_BODY_ERROR;
        }
        if (padding) {
            uint8_t *padding_bytes = malloc(padding);
            for (size_t k = 0; k < padding; k++) padding_bytes[k] = 0;
            fwrite(padding_bytes, padding, count, out);
            free(padding_bytes);
        }
    }

    free(bytes);
    return WRITE_OK;
}
