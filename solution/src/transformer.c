#include "bmp.h"
#include "image.h"
#include "transformer.h"

struct pixel get_pixel(struct image const img, size_t x, size_t y) {
    return img.data[img.width * x + y];
}

void set_pixel(struct image *img, size_t x, size_t y, struct pixel p) {
    img->data[img->width * x + img->width - 1 - y] = p;
}

struct image rotate(struct image const source) {
    struct image img = {
            .width = source.height,
            .height = source.width,
            .data = malloc(sizeof(struct pixel) * source.width * source.height)};
    for (size_t i = 0; i < source.height; i++)
        for (size_t j = 0; j < source.width; j++)
          set_pixel(&img, j, i, get_pixel(source, i, j));
    return img;
}
