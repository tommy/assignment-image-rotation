#include "bmp.h"
#include "image.h"
#include "transformer.h"


int main(int argc, char **argv) {
    if (argc != 3) return 0;

    FILE *original = fopen(argv[1], "rb");
    FILE *rotated = fopen(argv[2], "wb");
//    FILE *original = fopen("a.bmp", "rb");
//    FILE *rotated = fopen("b.bmp", "wb");

    struct image img;
    struct bmp_header head;
    from_bmp(original, &img, &head);
//    to_bmp(rotated, &img, &head);
    struct image rotated_img = rotate(img);
    to_bmp(rotated, &rotated_img, &head);

    free(img.data);
    free(rotated_img.data);

    fclose(original);
    fclose(rotated);
}
